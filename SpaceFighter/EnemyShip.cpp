
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{	// If delay second for enemy ship is more than 0, recalculating delay seconds using elapsed time since last frame.
		m_delaySeconds -= pGameTime->GetTimeElapsed();
		// If delay second for enemy ship is equal or less than 0, activating the ship
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{	// If ship is active, calculating active seconds using elapsed time since last frame
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); // if ship's activation second is more than 2 AND it is not on screen, deactivating the ship.
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{	// Sets initial position for ship's spawn
	SetPosition(position);
	// Sets delay second for ship's spawn
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{	// Determines if hit damage will destroy ship or not
	Ship::Hit(damage);
}